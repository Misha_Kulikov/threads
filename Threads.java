public class Threads {
    public static class HelloTheard extends Thread {
        public void run() {
            System.out.println("Hello from a thread");
        }

        public static void main(String args[]) {
            for (int i = 0; i < 10; i++) {
                (new HelloTheard()).start(i);
                System.out.println("Hello from main thread " + i);
            }

        }
        private void start(int i) {
        }
    }
}